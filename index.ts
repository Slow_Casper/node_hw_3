import FileDB from "./fileDB";

const newFileDB = new FileDB();

const newPostSchema = {
    id: Number,
    title: String,
    text: String,
    createData: Date,
};

// newFileDB.registerSchema("Second table", newPostSchema);

const newTable = newFileDB.getTable("Second table");

const newData1 = {
    title: "One table title",
    text: "This text is first in this posts"
};
const newData2 = {
    title: "Two table title",
    text: "This text is second in this posts"
};
const newData3 = {
    title: "Three table title",
    text: "This text is third in this posts"
};
const newData4 = {
    title: "Four table title",
    text: "This text is fourth in this posts"
};

const updatedData = {
    title: "Updated post title",
    text2: "This text will not pass"
};


(async () => {
    await newTable.getAll();
    // await newTable.create(newData1);
    // await newTable.create(newData2);
    // await newTable.create(newData3);
    // await newTable.create(newData4);
    await newTable.getById(1708893854501);
    await newTable.update(1708893854464, updatedData);
    await newTable.delete(1708893854468);
})()
