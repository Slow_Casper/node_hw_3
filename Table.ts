import fsPromise from 'fs/promises'
import path from "path";

interface TableData {
    [key: string]: string;
};

export class Table {
    private tableName: string;
    private filePath: string;

    constructor(tableName: string, filePath: string) {
        this.tableName = tableName;
        this.filePath = filePath;
    };

    async getAll(): Promise<any[]> {
        try {
            const data = await fsPromise.readFile(this.filePath, 'utf-8');

            console.log(JSON.parse(data));
            return JSON.parse(data);
        } catch (err) {
            console.log(err)
            return [];
        }
    };

    async getById(id: number): Promise<any | undefined> {
        try {
            const data = await fsPromise.readFile(this.filePath, 'utf-8');
            const parsedData = JSON.parse(data);
            const currentPost = parsedData.find((el: any) => el.id === id);

            console.log(currentPost);
            return currentPost;
        } catch (err) {
            console.log(err);
            return;
        };
    };

    async create(data: TableData): Promise<any> {
        const id = Date.now() + Math.floor(Math.random() * 100);
        const createDate = new Date();
        const newPost = { id, ...data, createDate};
        try {
          const fileData = await fsPromise.readFile(this.filePath, 'utf-8');
          const posts = JSON.parse(fileData);

          posts.push(newPost);

          await fsPromise.writeFile(this.filePath, JSON.stringify(posts, null, 2));

          console.log(`New post has been added`)
          return newPost;
        } catch (err) {
            console.log(err);
            return
        };
    };

    async update(id: number, newData: TableData): Promise<any | undefined> {    
        try {
            const fileData = await fsPromise.readFile(this.filePath, 'utf-8');
            const posts = JSON.parse(fileData);

            const postToUpdate = posts.find((el: any) => el.id === id)

            if(!postToUpdate) {
                console.log(`Post with id ${id} not found`);
                return;
            }
            Object.keys(newData).forEach(key => postToUpdate[key] = newData[key]);
            await fsPromise.writeFile(this.filePath, JSON.stringify(posts, null, 2));
  
            console.log(`The post has been updated`);
            return postToUpdate;
          } catch (err) {
              console.log(err);
              return
          };
    };

    async delete(id: number): Promise<number | undefined> {
        try {
            const fileData = await fsPromise.readFile(this.filePath, 'utf-8');
            const posts = JSON.parse(fileData);
            const deletedPostIndex = posts.findIndex((el: any) => el.id === id);

            const deletedPost = posts.splice(deletedPostIndex, 1)

            await fsPromise.writeFile(this.filePath, JSON.stringify(posts, null, 2));
            // const filteredPosts = posts.filter((el: any) => el.id !== id)
            
            // await fsPromise.writeFile(this.filePath, JSON.stringify(filteredPosts, null, 2));
  
            console.log(`The post has been deleted`)
            return deletedPost.id
          } catch (err) {
              console.log(err);
              return
          };
    };
};
