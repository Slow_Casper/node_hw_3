import path from "path";
import fsPromise from 'fs/promises'
import { Table } from "./Table";

interface Schema {
    [key: string]: StringConstructor | NumberConstructor | DateConstructor;
};

class FileDB {
    private schemas: { [tableName: string]: Schema };
    
    constructor() {
        this.schemas = {};
    };

    async registerSchema(tableName: string, schema: Schema) {
        try {
            const filePath = path.join(__dirname, `files/${tableName}.json`);
            await fsPromise.writeFile(filePath, '[]');
            this.schemas[tableName] = schema;
            
            console.log(`Schema for table ${tableName} has registered!`);
        } catch (error) {
            console.log(error);
        };
    };
    
    getTable(tableName: string) {
        const filePath = path.join(__dirname, `files/${tableName}.json`);
        return new Table(tableName, filePath);
    };
}

export default FileDB;
